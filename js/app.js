// Для чтения RequestAnimationFrame разными браузерами
(function() {
  var lastTime = 0;
  var vendors = ['ms', 'moz', 'webkit', 'o'];
  for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
      window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
      window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
                                 || window[vendors[x]+'CancelRequestAnimationFrame'];
  }

  if (!window.requestAnimationFrame)
      window.requestAnimationFrame = function(callback, element) {
          var currTime = new Date().getTime();
          var timeToCall = Math.max(0, 16 - (currTime - lastTime));
          var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
            timeToCall);
          lastTime = currTime + timeToCall;
          return id;
      };

  if (!window.cancelAnimationFrame)
      window.cancelAnimationFrame = function(id) {
          clearTimeout(id);
      };
}());

const line = document.querySelector('.line')
const labelTitle = document.querySelector('.label-title')
const logo = document.querySelector('.logo')
const socIcons = document.querySelector('.soc-icons')
const shadowDark = document.querySelector('.shadow-dark')
const bgRight = document.querySelector('.bg-right')
const bgLeft = document.querySelector('.bg-left')
const overlow = document.querySelector('.overflow')
const borderText = document.querySelector('.t-border')
const bgText = document.querySelector('.t-background')
const smokeRight = document.querySelector('.smoke-right')
const smokeMobileLeft = document.querySelector('.smoke-mobile-left')
const smokeLeft = document.querySelector('.smoke-left')
const manOne = document.querySelector('.man-1')
const manTwo = document.querySelector('.man-2')
const manThree = document.querySelector('.man-3')
const manFour = document.querySelector('.man-4')
const manMobileTwo = document.querySelector('.man-mobile-2')
const manMobileFour = document.querySelector('.man-mobile-4')

const startTime = new Date().getTime()

const styleBlocks = function() {
    line.style.opacity = 1
    logo.style.opacity = 1
    socIcons.style.opacity = 1
    labelTitle.style.opacity = 1
    bgRight.style.opacity = 1
    bgLeft.style.opacity = 1
    overlow.style.opacity = 1
    borderText.style.opacity = 1
    smokeRight.style.opacity = 1
    smokeLeft.style.opacity = 1
    manOne.style.opacity = 1
    manTwo.style.opacity = 1
    manThree.style.opacity = 1
    manFour.style.opacity = 1
    manMobileTwo.style.opacity = 1
    manMobileFour.style.opacity = 1
    bgText.style.opacity = 0.8

    window.requestAnimationFrame(styleBlocks)
}
styleBlocks()

if (window.matchMedia('(min-width: 1024px)').matches) {

    smokeLeft.style.left = -153 + 'px'
    smokeRight.style.right = -516 + 'px'

    bgLeft.style.left = 0
    bgRight.style.right = 0
}

if (window.matchMedia('(max-width: 360px)').matches) {
  smokeMobileLeft.style.bottom = 0
}

//Модальное окно

const modalTrigger = document.getElementsByClassName("trigger")[0]

const windowInnerWidth = document.documentElement.clientWidth
const scrollbarWidth = parseInt(window.innerWidth) - parseInt(document.documentElement.clientWidth)

const bodyElementHTML = document.getElementsByTagName("body")[0]
const modalBackground = document.getElementsByClassName("modalBackground")[0]
const modalBlur = document.getElementsByClassName("blur")[0]
const modalClose = document.getElementsByClassName("modalClose")[0]
const modalActive = document.getElementsByClassName("modalActive")[0]

function bodyMargin() {
    bodyElementHTML.style.marginRight = "-" + scrollbarWidth + "px"
    window.requestAnimationFrame(bodyMargin)
}

bodyMargin()

modalTrigger.addEventListener("click", function () {
    modalBackground.classList.add('open')
    if (windowInnerWidth >= 1020) {
        bodyMargin()
    }
})

modalClose.addEventListener("click", function () {
    modalBackground.classList.remove('open')
    if (windowInnerWidth >= 1366) {
        bodyMargin()
    }
}) 

modalBlur.addEventListener("click", function (event) {
    if (event.target === modalBlur) {
        modalBackground.classList.remove('open')
        if (windowInnerWidth >= 1020) {
            bodyMargin()
        }
    }
})